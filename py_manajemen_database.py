import sqlite3
from sqlite3 import Error
import json
from datetime import datetime
import time
DATABASE = 'database.db'

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
    
def select_project(tabel):
    con, cur = buka_db()
    con.row_factory = dict_factory
    cur = con.cursor()
    cur.execute(f"select * from {tabel} ORDER BY random() limit 1")
    hasil = cur.fetchall()
    con.close()
    return json.dumps(hasil)

def dotambahproject(nama,ket):
    conn = sqlite3.connect('database.db')
    sql = f"INSERT INTO project_ytb_uploader(nama_project, keterangan_project) VALUES('{nama}','{ket}')"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return cur.lastrowid

def buka_db():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    return conn, cursor

def dotambahdsource(data):
    conn = sqlite3.connect('database.db')
    sql = ''' INSERT INTO datasource_ytb_uploader(id_project, judul_video, path_video, diskripsi_video, setting_thumbnail, path_thumbnail, path_cookies)
              VALUES(?,?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql,data)
    conn.commit()
    return cur.lastrowid

def dotambahantrian(x):
    conn = sqlite3.connect('database.db')
    sqlcek  = f"select * from antrian_ytb_uploader where id_datasource = ({x})"
    sql = f"INSERT INTO antrian_ytb_uploader(id_datasource) VALUES({x})"
    cur = conn.cursor()
    cur.execute(sqlcek)
    hasil = cur.fetchall()
    if len(hasil) == 0:
        cur.execute(sql)
        conn.commit()
        return cur.lastrowid
    else:
        return "sudah ada"

def getlistrunbot():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    sql = f"SELECT a.id as 'id_antrian',d.* from antrian_ytb_uploader a inner join datasource_ytb_uploader d on a.id_datasource = d.id  where status = 0 or status = 3 limit 1"
    cur = conn.cursor()
    cur.execute(sql)
    hasil = cur.fetchall()
    conn.close()
    x = json.dumps(hasil)
    return x

def tulislog(id_datasource,text):
    datetime_str = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    conn = sqlite3.connect('database.db')
    sql = f"INSERT INTO log_ytb_uploader(id_datasource, waktu, log) VALUES({id_datasource},'{datetime_str}','{text}')"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return cur.lastrowid

def getlog():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    sql = f"select * from (SELECT * FROM log_ytb_uploader order by id DESC limit 100) order by id ASC"
    cur = conn.cursor()
    cur.execute(sql)
    hasil = cur.fetchall()
    conn.close()
    x = json.dumps(hasil)
    return x

def updatestatusds(id_datasource,status,url,keterangan):
    conn = sqlite3.connect('database.db')
    sql = f"UPDATE datasource_ytb_uploader SET status={status}, url_youtube='{url}', keterangan='{keterangan}' WHERE id={id_datasource}"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

def set_status_ds_ke_awal():
    conn = sqlite3.connect('database.db')
    sql = f"UPDATE datasource_ytb_uploader SET status=0 where status=1"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

def updatestatusupload(id_datasource,status,url,tanggal_upload,tanggal_schedule):
    conn = sqlite3.connect('database.db')
    sql = f"UPDATE datasource_ytb_uploader SET status={status}, url_youtube='{url}', tanggal_upload ='{tanggal_upload}', tanggal_schedule ='{tanggal_schedule}'  WHERE id={id_datasource}"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

def hapusantrian(id):
    conn = sqlite3.connect('database.db')
    sql = f"Delete from antrian_ytb_uploader where id={id}"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

def get_setting():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    sql = f"SELECT * from setting_aplikasi"
    cur = conn.cursor()
    cur.execute(sql)
    hasil = cur.fetchall()
    conn.close()
    x = json.dumps(hasil)
    return x

def setting_bot(status):
    conn = sqlite3.connect('database.db')
    sql = f"UPDATE setting_aplikasi SET settingan='{status}' WHERE id=1"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

# Ini untuk settingan video generator

def get_database(sql):
    conn = sqlite3.connect(DATABASE)
    cur = conn.cursor()
    cur.execute(sql)
    hasil = cur.fetchall()
    conn.close()

    return hasil

def update_database(sql):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

def insert_database(sql):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()

    return cur.lastrowid