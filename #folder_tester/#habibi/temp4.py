from threading import Thread
from time import sleep
import random
import sqlite3
import json

class ServerThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            s = self.get_status()
            s = json.loads(s)
            self.status = s['status_bot_youtubeuploader']
            self.antrian = s['antrian']
            self.dalam_proses = s['dalam_proses']
            self.ytb_upload_db(self.status,self.antrian,self.dalam_proses)
            sleep(1)
            
    def ytb_upload_db(self,status,antrian,dalam_proses):
        # x = [0,1]
        # status = random.choice(x) # 0 = bot tidak jalan, 1 = bot jalan
        # antrian = random.choice(x) # 0 = tidak ada antrian, 1 = ada antrian
        # dalam_proses = random.choice(x) # 0 = ada yang sedang proses, 0 = tidak ada yang diproses

        if self.status == "aktif" and self.antrian > 0 and self.dalam_proses == 0:
            print(f"Bisa upload | {self.status} / {self.antrian} / {self.dalam_proses}")
        else:
            print(f"Nggak bisa upload | {self.status} / {self.antrian} / {self.dalam_proses}")

    def get_status(self):
        conn = sqlite3.connect(r"E:\tools\Youtube Blaster GUI\youtube-blaster-gui\database.db")
        cur = conn.cursor()
        cur.execute(f'SELECT id, nama_settingan, settingan FROM setting_aplikasi WHERE id=1')
        status = cur.fetchall()
        cur.execute(f'SELECT count(*) as "antrian" FROM antrian_ytb_uploader')
        antrian = cur.fetchall()
        cur.execute(f'SELECT count(*) as "proses" FROM datasource_ytb_uploader where status=1')
        proses = cur.fetchall()
        conn.close()
        statusx = {status[0][1]:status[0][2],"antrian":antrian[0][0],"dalam_proses":1}
        status = json.dumps(statusx)
        return status

thread = ServerThread()
thread.start()
thread.join()