import sqlite3
from sqlite3 import Error
import json

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
    
def select_project(tabel):
    con, cur = buka_db()
    con.row_factory = dict_factory
    cur = con.cursor()
    cur.execute(f"select * from project_ytb_uploader")
    hasil = cur.fetchall()
    con.close()
    return json.dumps(hasil)

def buka_db():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    return conn, cursor

# def main():
#     db = manajemen_database()
#     print(db.select_project())

# if __name__ == '__main__':
#     main()