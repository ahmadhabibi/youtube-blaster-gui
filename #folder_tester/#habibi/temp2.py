import psutil

def if_process_is_running_by_exename(exename='chromedriver.exe'):
    for proc in psutil.process_iter(['pid', 'name']):
        # This will check if there exists any process running with executable name
        if proc.info['name'] == exename:
            return True
    return False

if if_process_is_running_by_exename():
    print("aa")