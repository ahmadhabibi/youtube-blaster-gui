import time
import threading
import random




c = 0

def count_loop():
    global c

    while True:
        c += 1
        time.sleep(1)

th = threading.Thread(target=count_loop)
th.start()

time.sleep(5)
print('Count:', c)

# Process will not exit, because thread is running and cannot end