import asyncio
import tornado.escape
import tornado.ioloop
import tornado.locks
import tornado.web
import os.path
import uuid
import random
import json
from tornado import gen

from tornado.options import define, options, parse_command_line
from manajemen_database import *

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="run in debug mode")


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.tabel = self.get_arguments("tabel")
        if self.tabel == []:
            # Handle me
            self.set_status(400)
            return self.finish("Invalid tabel")
        self.write(select_project(self.get_argument("tabel")))

def main():
    parse_command_line()
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
        ],
        debug=options.debug,
    )
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()