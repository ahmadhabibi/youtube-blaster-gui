import asyncio
import tornado.escape
import tornado.ioloop
import tornado.locks
import tornado.web
import os.path
import uuid
import random
import json
from tornado import gen
import time

from tornado.options import define, options, parse_command_line
from py_manajemen_database import *
from py_threading import *
from helper import *

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="run in debug mode")
DATABASE = 'database.db'

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT * FROM datasource_ytb_uploader')
        hasil = cur.fetchall()
        conn.close()
        self.render("niceadmin/ytb_dasboard.html",table=hasil)

class app_setting(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/app_setting.html")

class app_kontak(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/app_kontak.html")

class app_tutorial(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/app_tutorial.html")

class YuHalamanAntrian(tornado.web.RequestHandler):
    def get(self):
        STATUS = get_setting()
        STATUS = json.loads(STATUS)
        STATUS = STATUS[0]["settingan"]

        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT a.id,a.id_datasource,d.judul_video,d.status from antrian_ytb_uploader a inner join datasource_ytb_uploader d on a.id_datasource = d.id GROUP by a.id')
        hasil = cur.fetchall()
        conn.close()
        self.render("niceadmin/ytb_daftarantrian.html",antrian=hasil,status=STATUS)

class YuProjectList(tornado.web.RequestHandler):
    def get(self):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT P.*,count(D.id) as "jumlah_datasource" FROM project_ytb_uploader P left join datasource_ytb_uploader D on P.id_project = D.id_project GROUP BY P.id_project ')
        hasil = cur.fetchall()
        conn.close()
        self.render("niceadmin/ytb_dasboard.html",table=hasil)

class YuBotStatus(tornado.web.RequestHandler):
    def get(self):
        time.sleep(3)
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT id, nama_settingan, settingan FROM setting_aplikasi WHERE id=1')
        status = cur.fetchall()
        cur.execute(f'SELECT count(*) as "antrian" FROM antrian_ytb_uploader')
        antrian = cur.fetchall()
        cur.execute(f'SELECT count(*) as "proses" FROM datasource_ytb_uploader where status=1')
        proses = cur.fetchall()
        conn.close()
        statusx = {status[0][1]:status[0][2],"antrian":antrian[0][0],"dalam_proses":1}
        status = json.dumps(statusx)
        self.write(status)

class YuStatusBot(tornado.web.RequestHandler):
    def get(self,status):
        script = ''' <script type="text/javascript">
                var url = document.referrer;
                if(url == ''){
                url = "/"
                }
                window.location.replace(url);
                </script>'''
        if status == "aktif":
            setting_bot("aktif")
            self.write(script)
        elif status == "nonaktif":
            setting_bot("nonaktif")
            self.write(script)

class YuAntrianUpload(tornado.web.RequestHandler):
    def get(self):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT * FROM antrian_ytb_uploader limit 1')
        hasil = cur.fetchall()
        conn.close()
        if len(hasil) >= 1:
            status = {"status" : True, "id_antrian" : hasil[0][0], "id_datasource" : hasil[0][1]}
            status = json.dumps(status)
            self.write(status)
        else:
            status = {"status" : False}
            status = json.dumps(status)
            self.write(status)

class YuInputAntrian(tornado.web.RequestHandler):
    def get(self,id):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT * FROM datasource_ytb_uploader where id_project ={id} and status <> 2')
        hasil = cur.fetchall()
        conn.close()
        for i in hasil:
            dotambahantrian(i[0])
        time.sleep(5)
        self.redirect(f"/uploader/project/{id}")

class YuDeleteAntrian(tornado.web.RequestHandler):
    def get(self):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'delete from antrian_ytb_uploader where 1')
        hasil = cur.fetchall()
        conn.close()
        time.sleep(2)
        self.redirect(f"/uploader/antrian")

class YuProjectDetail(tornado.web.RequestHandler):
    def get(self,id):
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute(f'SELECT id,judul_video,path_video,diskripsi_video,setting_thumbnail,path_thumbnail,path_cookies,status FROM datasource_ytb_uploader where id_project = {id}')
        hasil = cur.fetchall()
        conn.close()
        self.render("niceadmin/ytb_project_detail.html",table=hasil,idproject=id)

class Yu_api_log(tornado.web.RequestHandler):
    def get(self):
        self.write(getlog())

class YuGetLog(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/ytb_log.html")

class menu_dashboard(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/dasboard_menu.html")

class YuTambahProject(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/ytb_tambahproject.html")

    def post(self):
        nama_project = self.get_body_argument("nama_project", default=None, strip=False)
        keterangan = self.get_body_argument("keterangan", default=None, strip=False)
        x = dotambahproject(nama_project,keterangan)
        if x > 0:
            self.redirect(f"/uploader/project/{x}")
        else:
            self.redirect("/uploader/tambahproject")

class YuTambahDataCSV(tornado.web.RequestHandler):
    def get(self,id):
        self.render("niceadmin/ytb_tambahds_csv.html",idproject=id)

class DoTambahDS(tornado.web.RequestHandler):
    def get(self):
        id_project = self.get_argument("id_project")
        judul_video = self.get_argument("judul_video")
        path_video = self.get_argument("path_video")
        diskripsi_video = self.get_argument("diskripsi_video")
        setting_thumbnail = self.get_argument("setting_thumbnail")
        path_thumbnail = self.get_argument("path_thumbnail")
        path_cookies = self.get_argument("path_cookies")
        data = (id_project, judul_video, path_video, diskripsi_video, setting_thumbnail, path_thumbnail, path_cookies)
        hasil = dotambahdsource(data)
        self.write(str(hasil))
        # print(hasil)
        # self.write(self.get_argument("idproject"))

class AmbilTabel(tornado.web.RequestHandler):
    def get(self):
        self.tabel = self.get_arguments("tabel")
        if self.tabel == []:
            # Handle me
            self.set_status(400)
            return self.finish("Invalid tabel")
        self.write(select_project(self.get_argument("tabel")))

class ytb_main(tornado.web.RequestHandler):
    def get(self):
        self.render("youtube_uploader_start.html")

class log_bot_uploader(tornado.web.RequestHandler):
    def get(self):
        self.render("log_bot.html")

# Video Generate

class vg_dashboard(tornado.web.RequestHandler):
    def get(self):
        data = get_database("SELECT id, nama, list_keyword, list_gambar, list_video, list_musik FROM project_video_blaster")
        self.render("niceadmin/VG_Dasboard.html",data=data)

class vg_sourcemusik(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/VG_SourceMusik.html")

class vg_sourcevideo(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/VG_SourceVideo.html")

class vg_videobaru(tornado.web.RequestHandler):
    def get(self):
        self.render("niceadmin/VG_VideoBaru.html")

# Video blaster

class list_project(tornado.web.RequestHandler):
    def get(self):
        list_project = get_data("/api/project")
        data = list_project['data']
        self.render("blaster/1_project_list.html",data=data)

class detail_project(tornado.web.RequestHandler):
    def get(self,id):
        data = get_data(f"/api/project/{id}")
        data = data['data'][0]
        self.render("blaster/2_detail_project.html",data=data)

class new_project(tornado.web.RequestHandler):
    def get(self):
        self.render("blaster/3_new_project.html")

class delete_project(tornado.web.RequestHandler):
    def get(self,id):
        self.render("blaster/4_delete_project.html",data=id)

class buat_ds(tornado.web.RequestHandler):
    def get(self,id):
        data = get_data(f"/api/project/{id}")
        if (data['status'] == True):
            data = data['data'][0]
            self.render("blaster/5_buat_ds.html",data=data)
        else:
            self.write(f"Anda tidak memiliki project dengan ID = {id}")

class detail_datasource(tornado.web.RequestHandler):
    def get(self,id):
        data = get_data(f"/api/datasource/ds/{id}")
        if (data['status'] == True):
            data=data['data'][0]
            self.render("blaster/7_detail_datasource.html",data=data)
        else:
            self.write("Datasource yang anda cari tidak ada, silahkan <a href='onclick=\"history.back()\"'>kembali</a>")

class delete_datasource(tornado.web.RequestHandler):
    def get(self,id):
        self.render("blaster/8_delete_datasource.html",data=id)

class datasource_perproject(tornado.web.RequestHandler):
    def get(self,id):
        data = get_data(f"/api/datasource/project/{id}")
        if (data['status'] == True): 
            data = data['data']
            self.render("blaster/6_datasource_perproject.html",data=data)
        else:
            self.write(f"Belum ada datasource, silahkan buat dahulu di <a href='/blaster/project/buat_ds/{id}'>Sini</a>")

            # cookies

class cookies_id(tornado.web.RequestHandler):
    def get(self,):
        self.render("blaster/cookies_id.html")

class new_cookies(tornado.web.RequestHandler):
    def get(self):
        self.render("blaster/new_cookies.html")

class update_cookies(tornado.web.RequestHandler):
    def get(self):
        self.render("blaster/cookies_update.html")

class list_cookies(tornado.web.RequestHandler):
    def get(self):
        list_project = get_data("/api/project")
        data = list_project['data']
        self.render("blaster/list_cookies.html",data=data)

class baca_log(tornado.web.RequestHandler):
    def get(self,last=1):
    	self.f = open("laporan.log", "r")
    	self.a = self.f.readlines()[last:]
    	self.f.close()
    	# self.laporan = self.a.replace("\n","<br />")
    	self.laporan = ""
    	for self.i in self.a:
    		self.laporan = f"{self.laporan}<br />{self.i}"
    	self.write(self.laporan)

class AmbilDataJSON(tornado.web.RequestHandler):
    def get(self):
        rand = random.random()
        data = {}
        data['random'] = rand
        datax = json.dumps(data)
        self.write(datax)

class bot(tornado.web.RequestHandler):
    def get(self):
        self.bot()
        self.write(a.aa())

    def bot(self):
        ytb_uploader()

class tanggal(tornado.web.RequestHandler):
    def get(self):
        from datetime import datetime
        import uuid

        self.tgl = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.randomtext = str(uuid.uuid4())
        self.write(f"{self.tgl}  |  {self.randomtext}")

def main():
    parse_command_line()
    # set_status_ds_ke_awal()
    # setting_bot("nonaktif")
    ''' Upload tread '''
    # T = Thread(target = runuploader)
    # T.start()

    ''' Tunnel '''
    # H = Thread(target = konek_ngrok)
    # H.start()

    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/app/setting", app_setting),
            (r"/app/kontak", app_kontak),
            (r"/app/tutorial", app_tutorial),

            (r"/blaster/project", list_project),
            (r"/blaster/project/(\d+)", detail_project),
            (r"/blaster/project/new", new_project),
            (r"/blaster/project/delete/(\d+)", delete_project),
            (r"/blaster/project/buat_ds/(\d+)",buat_ds),
            (r"/blaster/datasource/(\d+)", datasource_perproject),
            (r"/blaster/datasource/detail/(\d+)", detail_datasource),
            (r"/blaster/datasource/delete/(\d+)", delete_datasource),

            (r"/blaster/cookies", list_cookies),
            (r"/blaster/cookies/new", new_cookies),
            (r"/blaster/cookies/update", update_cookies),
            (r"/blaster/cookies/update/id", cookies_id),


            (r"/uploader/test", menu_dashboard),
            (r"/uploader/dasboard", YuProjectList),
            (r"/uploader/project/([0-9]+)", YuProjectDetail),
            (r"/uploader/tambahproject", YuTambahProject),
            (r"/uploader/tambahdatasource/([0-9]+)", YuTambahDataCSV),
            (r"/uploader/daftarantri/([0-9]+)", YuInputAntrian),
            (r"/uploader/antrian", YuHalamanAntrian),
            (r"/uploader/log",YuGetLog),
            (r"/generate/dasboard", vg_dashboard),
            (r"/generate/AddMusic", vg_sourcemusik),
            (r"/generate/AddVideo", vg_sourcevideo),
            (r"/generate/BuatBaru", vg_videobaru),
            (r"/api/statusbot/([aktif|nonaktif]+)",YuStatusBot),
            (r"/api/deleteantrian",YuDeleteAntrian),
            (r"/api/log",Yu_api_log),
            # (r"/api/runbot",YuRunBot),
            (r"/api/ds",DoTambahDS),
            (r"/api/botstatus",YuBotStatus),
            (r"/api/antrian",YuAntrianUpload),
            (r"/api",AmbilDataJSON),
            (r"/bot",bot),
            (r"/ytb_main",ytb_main),
            (r"/api/log",baca_log),
            (r"/api/tanggal",tanggal),
            (r"/api/tabel",AmbilTabel),
            # (r"/test",myfunc)
        ],
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        test="a",
        debug=options.debug,
    )
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()