from pyngrok import conf, ngrok
import os
import random
from threading import Thread
from py_manajemen_database import *
import requests
import sys
import subprocess
import json

app_setting = get_setting()
setting = json.loads(app_setting)
API_TELE = setting[4]["settingan"]
ID_TELE = setting[5]["settingan"]
auth = ["270FjWNUZZ8KIlLtgUkwcV14LLQ_7tDTDF9xbXv451KtWF7e1","272UsWmgHrWYm728hGByVZ6DT1f_2vS7KtWTvWXvEYAFf27KQ","272Zp2OtVP6OxGB5OPQ1fenfjAH_73xCAjCvtV6SxkbENf685","272aGTLNnyDnMwCNaCoCq6BGIYy_3pGSdRZBB9UYg5dyyYcci","272amRbm09UghBwRHEUMvNeVrz8_69m3AaWGg2EojYkxAUdGL","272buKXU7JTujpV0CKIc5L5PML2_3QuduRb16nyoThWkz4ccB","272cSBeFr5AA6S4UARHabkIIWH0_2jiQ3Es7kMgD2BkVXJNTG","272cr0WyGfzQTCzP0xn90xHszbT_4fQEh5wrx3z2nrvDR5ihN","272dIwN0ftETE58rqZaH7lkPo4d_4NTMR2yKvPuHC5QFAUL8s","272dlOjrpvJcEfzcCePm2RQXgZe_4fbLD1L9tuutNQ76JBGLY","272eT6FL8RRhjC4uJbDjA0spzfv_6WmJm7op2hJgaBDDDjvxN"]
conf.get_default().ngrok_path = os.path.join(os.getcwd(),"module/ngrok.exe")


''' Untuk mengirim laporan ke telegram '''
def telegram_send(bot_message):
    bot_token = API_TELE
    bot_chatID = ID_TELE
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    res = response.json()
    return res['ok']

def konek_ngrok():
	ngrok.set_auth_token(random.choice(auth))
	ngrok_tunnel = ngrok.connect(8888)
	url = ngrok_tunnel.public_url
	pesan = f'Tools jalan dengan URL : {url}'
	telegram_send(pesan)
	print(pesan)

	while True:
		continue


def get_os():
	s = sys.platform
	so = ""
	if 'darwin' in s:
		so = "osx"
	elif 'win32' in s:
		so = "win"
	elif 'linux' in s:
		so = 'linux'

	return so


if get_os() == "osx":
	ffmpeg = os.path.join(os.getcwd(),"module","osx","ffmpeg")
	ffprobe = os.path.join(os.getcwd(),"module","osx","ffprobe")
	ffplay = os.path.join(os.getcwd(),"module","osx","ffplay")
	ngrok = os.path.join(os.getcwd(),"module","osx","ngrok")

import shlex
def durasi(y):
	x = f"{ffprobe} -v quiet -print_format json -show_streams '{y}'"
	x = shlex.split(x)
	x = subprocess.check_output(x).decode('utf-8')
	x = json.loads(x)
	return (x['streams'][0]['duration'])

def get_data(url):
	url = f"http://localhost:3000{url}"
	data = requests.get(url)
	return data.json()

# list_project = get_data("/api/project")
# for i in list_project['data']:
# 	print(i['id'],i['nama'])