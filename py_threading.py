from threading import *
from time import sleep
import random
import sqlite3
import json

from py_manajemen_database import *
from ytb_uploader_new import *

def runuploader():
    while True:
        x,y,z = get_status()
        status = x
        antrian = y
        dalam_proses = z
        ytb_upload_db(status,antrian,dalam_proses)
        sleep(1)
            
def ytb_upload_db(status,antrian,dalam_proses):
    if status == "aktif" and antrian > 0 and dalam_proses == 0:
        # print(f"Bisa upload | {status} / {antrian} / {dalam_proses}")
        listrunbot = getlistrunbot()
        if listrunbot.count == 0:
            print("Habis")
        else:
            datasource = json.loads(listrunbot)[0]
            id_antrian = datasource["id_antrian"]

            ytb_upload_from_database()
            hapusantrian(id_antrian)
    else:
        sleep(0.1)
        # print(f"Nggak bisa upload | Status bot : {status} / Antrian : {antrian} / dalam proses : {dalam_proses}")

def get_status():
    conn = sqlite3.connect(r"E:\tools\Youtube Blaster GUI\youtube-blaster-gui\database.db")
    cur = conn.cursor()
    cur.execute(f'SELECT id, nama_settingan, settingan FROM setting_aplikasi WHERE id=1')
    status = cur.fetchall()
    cur.execute(f'SELECT count(*) as "antrian" FROM antrian_ytb_uploader')
    antrian = cur.fetchall()
    cur.execute(f'SELECT count(*) as "proses" FROM datasource_ytb_uploader where status=1')
    proses = cur.fetchall()
    conn.close()
    return status[0][2],antrian[0][0],proses[0][0]



# thread = ServerThread()
# thread.start()
# thread.join()
# T = Thread(target = runuploader)
# T.start() 
# print('a')