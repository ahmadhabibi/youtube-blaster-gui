# Rancangan API Tools Video Blaster

Berikut ini adalah beberapa catatan untuk pembuatan API tools video blaster. Tujuan dibuat document ini adalah untuk memudahkan dalam pembuatan API.

## Rancangan API

### 1. PROJECT

| # | Nama API | Perintah | URL | Keterangan | SQL
| --- | --- | --- | --- | --- | ---
| 1.1 | List project | GET | /api/project | Untuk mengambil data / list project. Agar tidak berat, maka ambil data yang penting-penting saja, untuk data legkapnya bisa di detail
| 1.2 | Detail project (by id) | GET | /api/project/\<id_project> | Untuk mengambil detail project untuks setiap project, berdasarkan \<id_project>
| 1.3 | Buat project baru | POST | /api/project/add | untuk membuat project baru
| 1.4 | Update project | PUT | /api/project/update/\<id_project> | Untuk update project
| 1.5 | Delete project | DELETE | /api/project/delete/\<id_project> | Untuk mengahpus project

### 2. DATASOURCE

| # | Nama halaman | Perintah | Rencana URL | Penjelasan |
| --- | --- | --- | --- | --- |
| 2.1 | List datasource | GET | /api/datasource | berisi list project yang sudah dibuatkan datasourcenya, ketika diklik akan menuju ke halaman data source yang dikelompokan sesuai dengan projectnya
| 2.2 | List datasource per project | GET | /api/datasource/\<id_project> | berisi list datasource untuk setiap project
| 2.3 | Update datasource | PUT | /api/datasource/update/\<id_ds> | API untuk update datasource
| 2.4 | Hapus datasource | DELETE | /api/datasource/delete/\<id_ds> | API untuk delete datasource
| 2.5 | Input datasource | POST | /api/datasource/new | Input datasource baru
| 2.6 | Ambil datasource per datasource | GET | /api/datasource/1/\<id_ds> | Input datasource baru

### 3. COOKIES

| # | Nama halaman | Perintah | Rencana URL | Penjelasan |
| --- | --- | --- | --- | --- |
| 3.1 | List cookies | GET | /api/cookies | Berisi list akun / cookies yang sudah diinput ke database |
| 3.2 | Tambah cookies | POST | /api/cookies/new | Untuk input cookies baru ke database  |
| 3.3 | Update cookies | PUT | /api/cookies/update/\<id_cookies> | Untuk update cookies
| 3.4 | Label cookies | GET | /api/cookies/label | Berisi list label akun, ketika di klik akan masuk ke menu update
| 3.5 | Buat label cookies | POST | /api/cookies/label/new | *Form* untuk membuat label cookies baru
| 3.6 | Update label cookies | PUT | /api/cookies/label/update/\<id_label_cookies> | *Form* untuk update cookies
| 3.7 | Ambil detail cookies | GET | /api/cookies/\<id_cookies> | *Form* untuk membuat label cookies baru
| 3.8 | Ambl detaol label cookies | GET | /api/cookies/label/\<id_label_cookies> | *Form* untuk update cookies