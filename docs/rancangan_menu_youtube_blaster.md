# Rancangan Menu Youtube Blaster
---

Untuk versi terbaru ini nanti, tools sudah bisa generate video sendiri dan langsung upload ke youtube secara otomatis.

## Alur proses
 | # | Proses | Penjelasan |
 | --- | --- | ---- |
 | 1 | Setting project | Setting project secara lengkap mulai dari nama_project, keyword, list video slideshow, setting deskripsi youtube, setting cookies, dll | 
 | 2 | Generate datasource | Menggenerate data source dari data-data yang ada di project dengan jumlah sesuai dengan settingan, misal dimasukan jumalhnya 100, maka akan membuat 100 data source. Jika dimasukan 1000, maka akan membuat 1000 data source. | 
 | 3 | Mengaktifkan bot render & upload | Mengaktifkan bot render dan upload, maka tools akan berjalan secara background dan akan melakukan proses yang sudah kita setting. Alurnya adalah cek apakah ada datasource yang perlu di proses. Kalau ada, langkah pertama yaitu download video intro dan thumbnail, selanjutnya membuat video unik dari slideshow yang sudah kita sediakan. Lalu mengupload ke youtube yang sudah kita tentukan cookiesnya | 
 | 4 | Update database | Proses nomor 2 di atas, setiap poinnya akan secara otomatis update data ke database. Misal tools berhasil membuat video intro, maka tools akan secara otomatis update status di database kalau sudah selesai membuat video intro. Begitu juga yang lain | 
 | 5 | Lihat log | Karena proses ke 4 berjalan otomatis. Maka kita bisa melihat proses yang berjalan, di halaman log | 

## Halaman menu yang perlu dibuat

### 1. Project
| # | Nama halaman | Rencana URL | Penjelasan |
| --- | --- | --- | --- |
| 1.1 | List project | /blaster/project | Berisi tabel list project. Bagian nama project bisa diklik dan mengarahka ke detail project. Ada juga keterangan berapa video yang akan diproses, sudah dibuat, sudah terupload, dan yang error.
| 1.2 | Detail project | /blaster/project/\<id_project> | Berisi detail project untuk project tertentu (sesuai \<id_project>). Isi seperti nama, list keyword, list video slideshow, deskripsi, cookis dll
| 1.3 | Buat project baru | /blaster/project/new | *Form* untuk input project baru
| 1.4 | Halaman update project | /blaster/project/update/\<id_project> | *Form* untuk update project
| 1.5 | Halaman konfirmasi delete | /blaster/project/delete/\<id_project> | *From* konfirmasi untuk delete project beserta data-data yang lain


### 2. Data source upload
| # | Nama halaman | Rencana URL | Penjelasan |
| --- | --- | --- | --- |
| 2.1 | List datasource | /blaster/datasource | berisi list project yang sudah dibuatkan datasourcenya, ketika diklik akan menuju ke halaman data source yang dikelompokan sesuai dengan projectnya
| 2.2 | List datasource per project | /blaster/datasource/\<id_project> | berisi list datasource untuk setiap project
| 2.3 | Update datasource | /blaster/datasource/update/\<id_ds> | *Form* untuk update datasource
| 2.4 | Halaman konfirmasi hapus datasource | /blaster/datasource/delete/\<id_ds> | *Form* konfirmasi untuk delete datasource


### 3. Cookies
| # | Nama halaman | Rencana URL | Penjelasan |
| --- | --- | --- | --- |
| 3.1 | List cookies | /blaster/cookies | Berisi list akun / cookies yang sudah diinput ke database |
| 3.2 | Tambah cookies | /blaster/cookies/new | *Form* untuk input cookies baru ke database  |
| 3.3 | Update cookies | /blaster/cookies/update/\<id_cookies> | *Form* untuk update cookies
| 3.4 | Label cookies | /blaster/cookies/label | Berisi list label akun, ketika di klik akan masuk ke menu update
| 3.5 | Buat label cookies | /blaster/cookies/label/new | *Form* untuk membuat label cookies baru
| 3.6 | Update label cookies | /blaster/cookies/label/update/\<id_label_cookies> | *Form* untuk update cookies

### 4. Log
| # | Nama halaman | Rencana URL | Penjelasan |
| --- | --- | --- | --- |
| 4.1 | Halaman log | /blaster/log | Halaman yang berisi log tools

## Detail rancangan halaman

### 1.1 list project

[Tombol tambah project]

Berisi tabel dengan susunan seperti dibawah ini

| # | Nama Project | Belum diproses | Buat video | Berhasil upload |
| --- | --- | --- | --- | --- |
| 1 | Dummy | Dummy | Dummy | Dummy |
| 2 | Dummy | Dummy | Dummy | Dummy |
| 3 | Dummy | Dummy | Dummy | Dummy |
| 4 | Dummy | Dummy | Dummy | Dummy |
| 5 | Dummy | Dummy | Dummy | Dummy |

### 1.2 Detail project
> Berisi detail project untuk project tertentu (sesuai \<id_project>). Isi seperti nama, list keyword, list video slideshow, deskripsi, cookies dll

**Susunan halaman**

Nama Project

[Tombol update project] [Tombol buat datasource]

List keyword (`Berupa tabel scroll`)

| # | Keyword |
| --- | --- |
| 1 | Contoh keyword |
| 2 | Contoh keyword |
| 3 | Contoh keyword |
| 4 | Contoh keyword |
| 5 | Contoh keyword |
| x | Contoh keyword |

List Video Slideshow (`Berupa tabel scroll`)

| # | Path Video Slideshow |
| --- | --- |
| 1 | Contoh path video slideshow |
| 2 | Contoh path video slideshow |
| 3 | Contoh path video slideshow |
| 4 | Contoh path video slideshow |
| 5 | Contoh path video slideshow |
| x | Contoh path video slideshow |

List Gambar (`Berupa tabel scroll`)

| # | Path Gambar |
| --- | --- |
| 1 | Contoh path gambar |
| 2 | Contoh path gambar |
| 3 | Contoh path gambar |
| 4 | Contoh path gambar |
| 5 | Contoh path gambar |
| x | Contoh path gambar |

Deskripsi (`Berupa text area yang tidak bisa diedit`)

[cukup jelas]


Cookies (`Berupa tabel scroll`)

| # | Label cookies | Nama cookies
| --- | --- | --- |
| 1 | Contoh label cookies | Contoh nama cookies
| 2 | Contoh label cookies | Contoh nama cookies
| 3 | Contoh label cookies | Contoh nama cookies
| 4 | Contoh label cookies | Contoh nama cookies
| 5 | Contoh label cookies | Contoh nama cookies
| x | Contoh label cookies | Contoh nama cookies

### 1.3 Buat project baru

`Form untuk input project baru`

* Nama project -> input text
* List keyword -> text area
* Path gambar -> text area
* Path video slideshow -> text area
* Deskripsi -> text area
* Cookies -> select / pilihan (ambil data dari label)

### 1.4 Halaman update project
`Form untuk update project, isi halamanya sama seperti halaman 1.3`

### 1.5 Halaman konfirmasi delete

`Halaman konfirmasi delete project`

Apakah anda yakin mau menghapus project {nama_project}?

[Ya] [Batal]

### 2.1 List datasource
`Berisi list project yang sudah dibuatkan datasourcenya, ketika diklik akan menuju ke halaman data source yang dikelompokan sesuai dengan projectnya`

| # | Nama Project | Jumlah Datasource
| --- | --- | ---
1 | Project 1 | 10
2 | Project 2 | 100
... | Project x | 1000

### 2.2 List datasource per project
`Berisi list datasource untuk setiap project`

| # | Keyword | Status | Url Image | Deskripsi | Cookies | File video | File thumbnail | Url Youtube | Tanggal proses | Tanggal upload
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
| 1 | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy
| 2 | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy
| 3 | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy
| 4 | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy
| 5 | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy | Dummy

### 2.3 Update datasource
`Form untuk update datasource`

Berisi form dengan inputan untuks semua kolom 2.2

### 2.4 Halaman konfirmasi hapus datasource
`Form konfirmasi untuk delete datasource`

Apakah anda yakin mau menghapus datasource ini?

[ya] [batal]

### 3.1 List cookies
`Berisi list akun / cookies yang sudah diinput ke database`

| # | Nama Akun | Status | Action |
| --- | --- | --- | --- |
| 1 | Akun Kubah 1 | Aktif | [update] [hapus]
| 2 | Akun Kubah 1 | Non-aktif | [update] [hapus]
| 3 | Akun Kubah 1 | Aktif | [update] [hapus]
| 4 | Akun Kubah 1 | Aktif | [update] [hapus]
| 5 | Akun Kubah 1 | Aktif | [update] [hapus]


### 3.2 Tambah cookies
`Form untuk input cookies baru ke database`

* Nama akun -> inputan
* Json Cookies -> Text area

[Simpan cookies] [Reset]

### 3.3 Update cookies
`Form untuk update cookies`

* Nama akun -> inputan
* Json Cookies -> Text area

[Simpan cookies] [Reset]

### 3.4 Label cookies
`Berisi list label akun, ketika di klik akan masuk ke menu update`

| # | Label | Qty Cookies | Cookies Aktif | Cookies Non-aktif | Action
| --- | --- | --- | --- | --- | ---
| 1 | Akun kubah | 10 | 9 | 1 | [Hapus label] [Update label]
| 2 | Akun kubah | 10 | 9 | 1 | [Hapus label] [Update label]
| 3 | Akun kubah | 10 | 9 | 1 | [Hapus label] [Update label]
| 4 | Akun kubah | 10 | 9 | 1 | [Hapus label] [Update label]
| 5 | Akun kubah | 10 | 9 | 1 | [Hapus label] [Update label]

### 3.5 Buat label cookies
`Form untuk membuat label cookies baru`

* Nama Label -> text input
* Pilihan cookies -> Multiple select

[Buat label cookies] [Reset]

### 3.6 Update label cookies
`Form untuk update cookies`

* Nama Label -> text input
* Pilihan cookies -> Multiple select

[Update label cookies] [Reset]

### 4.1 Halaman log

`Halaman yang berisi log tools`

Berupa kotakan yang isinya div dengan scroll -> berisi text log yang terus update