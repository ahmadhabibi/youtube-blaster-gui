# Struktur Database untuk Aplikasi ini
---------------------------------------

## Strutur Tabel
1. setting_aplikasi
2. project_ytb_uploader
3. datasource_ytb_uploader
4. log_ytb_uploader

## Struktur Kolom

### setting_aplikasi
* id | format int
* nama_settingan | str
* settingan | text

### project_ytb_uploader
* id_project
* nama_project
* keterangan_project

### datasource_ytb_uploader
* id | format int auto increment
* id_project | join dengan `project_ytb_uploader`
* judul_video
* path_video
* diskripsi_video
* setting_thumbnail
* path_thumbnail
* path_cookies


### log_ytb_uploader
* id | format int auto increment
* id_project | join dengan `project_ytb_uploader`
* waktu
* log

