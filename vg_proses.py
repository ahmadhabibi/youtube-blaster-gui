from py_manajemen_database import *
from helper import *
from urllib import request,parse
import requests
import random
from datetime import datetime
import string
from time import sleep
import subprocess
import shlex

import threading

"""
Status render
0 : belum diproses sama sekali
1 : dalam proses
2 : berhasil download intro & thumbnail
3 : error download intro & thumbnail
4 : berhasil generate video
5 : error render
"""

# Setting folder temp
folder_temp = os.path.join(os.getcwd(),"temp")
FFMPEG = "ffmpeg"
FFPROBE = ffprobe
FOLDER_HASIL = folder_temp

"""
===========================
Function untuk render video
===========================
"""

def runBash(command):
	# Ini fungsinya untuk execute command ke CMD
	c = shlex.split(command)
	hasil=subprocess.Popen(c,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
	new=hasil.stdout.readlines()
	time.sleep(0.5)

def tulis_log(text):
	# Ini fungsinya untuk menulis log
	dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
	tulisan = "{} | {}".format(dt_string,text)
	print(tulisan)

# Function untuk render video dari list video
def render_join_video(nama_video,list_video,intro):
	path_file_txt = os.path.join(FOLDER_HASIL,"{}.txt".format(nama_video))
	path_file_video = os.path.join(FOLDER_HASIL,"{}-part1.mp4".format(nama_video))

	f = open(path_file_txt, "w")
	strteks = f"file '{intro}'\n"
	f.write(strteks)

	for i in range(0,5):
		strteks = f"file '{random.choice(list_video)}'\n"
		f.write(strteks)
	f.close()

	tulis_log("Video_generator_2022.exe | Render_join_Video | Berhasil membuat file {}".format(path_file_txt))
	strbash = FFMPEG+' -y -f concat -safe 0 -i "{}" -c copy "{}"'.format(path_file_txt,path_file_video)
	runBash(strbash)
	tulis_log("Video_generator_2022.exe | Render_join_Video | Render video : {}".format(path_file_video))
	time.sleep(3)
	os.remove(path_file_txt)
	tulis_log(f"Video_generator_2022.exe | Render_join_Video | Berhasil membuat video {path_file_video}")
	time.sleep(3)

def waktu(detik):
	b = time.strftime('%H:%M:%S', time.gmtime(detik))
	return b

#baca durasi video
def durasi(y):
	x = f"{FFPROBE} -v quiet -print_format json -show_streams '{y}'"
	x = shlex.split(x)
	x = subprocess.check_output(x).decode('utf-8')
	x = json.loads(x)
	return int(float(x['streams'][0]['duration']))

# Function untuk menambahkan audio
def crop_musik(video,musikinput,musikoutput):
	panjang_video = durasi(video)
	panjang_musik = durasi(musikinput)
	start = waktu(random.randrange(0, panjang_musik - 30))
	end = waktu(panjang_video)
	path = os.path.join(FOLDER_HASIL,musikoutput)
	str = FFMPEG+" -y -ss " + start + " -i \"" + musikinput + "\" -t " + end + " -c copy \""+ path+"\""
	runBash(str)

def tambah_musik(video,musik):
	file_musik_baru = os.path.basename(musik).replace(".mp3","-temp.mp3")
	file_video_baru = os.path.basename(video.replace("part1","part2"))
	path_musik_baru = os.path.join(FOLDER_HASIL,file_musik_baru)
	path_video_baru = os.path.join(FOLDER_HASIL,file_video_baru)
	
	crop_musik(video,musik,file_musik_baru)
	time.sleep(2)
	str = FFMPEG+" -y -i \""+video+"\"  -i \""+path_musik_baru+"\" -c copy -map 0:v:0 -map 1:a:0 -shortest \""+path_video_baru+"\""
	print(str)
	tulis_log("Video_generator_2022.exe | Tambah_audio | Menambahkan musik ke video : {}".format(path_video_baru))
	runBash(str)
	time.sleep(2)
	tulis_log("Video_generator_2022.exe | Tambah_audio | Berhasil menambahkan musik ke video")

#Menambahkan watermark video green screen #3BBD1E
def watermark_video(video,wtr,start="0"): 
	output = os.path.basename(video)
	output = os.path.join(FOLDER_HASIL, output.replace("par1","part3").replace("part2","part3"))
	str = FFMPEG+" -y -i \""+video+"\" -i \""+wtr+"\" -filter_complex \"[1:v]colorkey=0x3BBD1E:0.3:0.2[ckout];[ckout]setpts=PTS-STARTPTS+"+start+"/TB[ckout];[0:v][ckout]overlay=0:0:eof_action=pass[out]\" -map \"[out]\" -map 0:a -c:a copy \""+ output+"\""
	# print(str)
	#print("Watermark video : {}".format(output))
	tulis_log("Video_generator_2022.exe | Watermark video / lowerthird | {}".format(output))
	runBash(str)
	time.sleep(5)
	tulis_log("Video_generator_2022.exe | Watermark video / lowerthird berhasil")

#Menambahkan watermark gambar
def watermark_gambar(video,image,start="0"): 
	output = os.path.basename(video)
	output = os.path.join(FOLDER_HASIL, output.replace("par1","part3").replace("part2","part3"))
	str = FFMPEG+" -y -i \""+video+"\" -i \""+image+"\" -filter_complex \"[1]setpts=PTS-STARTPTS+"+start+"/TB[img];[0:v][img]overlay=0:0\" -c:a copy -c:v libx264 -preset veryfast \""+ output+"\""
#     print(str)
	#print("Watermark video : {}".format(output))
	tulis_log("Video_generator_2022.exe | Watermark gambar : {}".format(output))
	runBash(str)
	time.sleep(5)
	tulis_log("Video_generator_2022.exe | Watermark gambar berhasil")


"""
Function

"""

def random_id(N=5):
	return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(N))

def download_video(judul,url_gambar,kode):
	data = {"judul" : judul, "url_gambar" : url_gambar}
	data = parse.urlencode(data)
	url_render = f"http://194.233.90.78:8000/render?{data}"
	url_video = f"http://194.233.90.78:8000/video?{data}"
	url_thumbnail = f"http://194.233.90.78:8000/thumbnail?{data}"
	
	try:
		rend = requests.get(url_render, timeout=300)
		if rend.status_code == 200:
			request.urlretrieve(url_video,os.path.join(folder_temp,f"{judul}_{kode}.mp4"))
			request.urlretrieve(url_thumbnail,os.path.join(folder_temp,f"{judul}_{kode}.jpeg"))
			return f"Sukses : Donwload {judul}_{kode}"
			
	except Exception as e:
		return f"Error : {e}"

def download_intro_thumbnail():
	d = "select * from render_video_blaster where status = 0 or status = 3 limit 1"
	d = get_database(d)

	if d:
		d = d[0]
		kode = random_id()
		dl = download_video(d[1],d[2],kode)

		if "Sukses" in dl:
			vid = os.path.join(folder_temp,f"{d[1]}_{kode}.mp4")
			thumb = os.path.join(folder_temp,f"{d[1]}_{kode}.jpeg")
			s = f"UPDATE render_video_blaster SET status=2, path_video='{vid}', path_thumbnail='{thumb}' WHERE id_render={d[0]}"
		else:
			s = f"UPDATE render_video_blaster SET status=3 WHERE id_render={d[0]}"
		update_database(s)

def render_video_panjang():
	d = "SELECT rvb.id_render,rvb.keyword,rvb.path_video,rvb.id_project,pvg.list_video,pvg.list_musik from render_video_blaster rvb inner join project_video_generator pvg on rvb.id_project = pvg.id where status = 2 limit 1"
	d = get_database(d)

	if d:
		data = d[0]
		id_render = data[0]
		keyword = data[1]
		intro = data[2]
		id_project = data[3]
		list_video_slide = data[4].split("\n")
		file_audio = random.choice(data[5].split("\n"))
		setting_audio = 1
		setting_watermark = 0

		try:
			s = f"UPDATE render_video_blaster SET status=1 WHERE id_render={id_render}"
			update_database(s)

			nama_file = f"{id_render}_{keyword}"
			file_hasil = os.path.join(FOLDER_HASIL,"{}.mp4".format(nama_file))

			tulis_log(f"Video_generator_2022.exe | Memulai membuat video {file_hasil}")
			render_join_video(nama_file,list_video_slide,intro)
			
			video_temp = os.path.join(FOLDER_HASIL,"{}-part1.mp4".format(nama_file))
			time.sleep(5)
			
			if setting_audio == "1" or setting_audio == 1:
				tambah_musik(video_temp,file_audio)
				video_temp = os.path.join(FOLDER_HASIL,"{}-part2.mp4".format(nama_file))

			time.sleep(5)
			if setting_watermark == "1" or setting_watermark == 1:
				watermark_gambar(video_temp,file_watermark,start_watermark)
				video_temp = os.path.join(FOLDER_HASIL,"{}-part3.mp4".format(nama_file))
				time.sleep(5)
			elif setting_watermark == "2" or setting_watermark == 2:
				watermark_video(video_temp,file_watermark,start_watermark)
				video_temp = os.path.join(FOLDER_HASIL,"{}-part3.mp4".format(nama_file))
				time.sleep(5)
			else:
				tulis_log("Tanpa watermark")

			os.rename(video_temp, file_hasil)

			files_in_directory = os.listdir(FOLDER_HASIL)
			filtered_files = [file for file in files_in_directory if "part" in file or "txt" in file or "temp" in file]
			for file in filtered_files:
				path_to_file = os.path.join(FOLDER_HASIL, file)
				os.remove(path_to_file)
			
			tulis_log(f"Video_generator_2022.exe | Berhasil membuat video {file_hasil}")

			s = f"UPDATE render_video_blaster SET status=4, final_video='{file_hasil}' WHERE id_render={id_render}"
			update_database(s)
		except Exception as e:
			s = f"UPDATE render_video_blaster SET status=5 WHERE id_render={id_render}"
			update_database(s)


# download_intro_thumbnail()
# render_video_panjang()

def run_dl():
	while True:
		download_intro_thumbnail()

def run_render():
	while True:
		render_video_panjang()

dl = Thread(target = run_dl)
dl.start()

ren = Thread(target = run_render)
ren.start()

