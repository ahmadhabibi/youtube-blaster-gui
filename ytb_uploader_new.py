import json
from datetime import datetime
from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.chrome.options import Options

import logging
import random
import re
import requests
import os
import sys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from datetime import datetime,timedelta
import urllib

from py_manajemen_database import *


# ----setting config----
cwd = os.getcwd()

# Ambil settingan dari database
app_setting = get_setting()
setting = json.loads(app_setting)

STATUS_BOT = setting[0]["settingan"]
CHROME = setting[1]["settingan"]
STATUS = int(setting[2]["settingan"])
RANDOM_HARI = int(setting[3]["settingan"])
API_TELE = setting[4]["settingan"]
ID_TELE = setting[5]["settingan"]
JEDA_UPLOAD = int(setting[6]["settingan"])

''' Untuk menulis log '''
def fungsi_tulis(id_datasource,text):
    # Ambil function dari py_manajemen_database
    tulislog(id_datasource,text)

''' Untuk mengirim laporan ke telegram '''
def telegram_send(bot_message):
    bot_token = API_TELE
    bot_chatID = ID_TELE
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    res = response.json()
    return res['ok']

""" Function untuk login dengan cookies """
def domain_to_url(domain: str) -> str:
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str,id_datasource):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                fungsi_tulis(id_datasource,f"Couldn't set cookie {cookie['name']} for {domain}")


""" Function untuk upload video """
def _wait_for_processing(id_datasource,driver):
    progress_label: WebElement = driver.find_element_by_css_selector("span.progress-label")
    pattern = re.compile(r"(finished processing)|(Checks complete.*)|(processing hd.*)|(check.*)|(upload complete.*)")
    current_progress = progress_label.get_attribute("textContent")
    last_progress = None
    
    while not pattern.match(current_progress.lower()):
        if last_progress != current_progress:
            fungsi_tulis(id_datasource,f'Current progress: {current_progress}')
        last_progress = current_progress
        sleep(5)
        current_progress = progress_label.get_attribute("textContent")

def upload_file(driver: WebDriver, path_video: str,description: str,setting_thumbnail: str, thumbnail_path: str, title: str, id_datasource : int): 
    fungsi_tulis(id_datasource,"buka menu upload")
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "ytcp-icon-button#upload-icon"))).click()
    fungsi_tulis(id_datasource,'click upload file')
    video_input = driver.find_element_by_xpath('//input[@type="file"]')
    video_input.send_keys(path_video)
    fungsi_tulis(id_datasource,'sedang mengupload video')
    sleep(10)
    
    # Mengganti judul
    fungsi_tulis(id_datasource,"menulis judul")
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//ytcp-social-suggestions-textbox[@id='title-textarea']//div[@id='textbox']")))
    title_input: WebElement = driver.find_element_by_xpath(
        "//ytcp-social-suggestions-textbox[@id='title-textarea']//div[@id='textbox']")
    title_input.send_keys(Keys.CONTROL + 'a', Keys.BACKSPACE)
    title_input.send_keys(title)

    # Menulis deskripsi video
    fungsi_tulis(id_datasource,"Membuat deskripsi")
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, '//ytcp-social-suggestions-textbox[@label="Description"]//div[@id="textbox"]')))
    deskripsi_input: WebElement = driver.find_element_by_xpath( 
        '//ytcp-social-suggestions-textbox[@label="Description"]//div[@id="textbox"]')
    deskripsi_input.send_keys(description)
    fungsi_tulis(id_datasource,"membuat deskripsi done")

    # Setting thumbnail
    # // 1 = upload menggunakan thumbnail
    # // 0 = upload tidak menggunakan thumbnail

    if setting_thumbnail == 1:
        fungsi_tulis(id_datasource,"setting thumbnail")
        WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//button[@class='remove-default-style style-scope ytcp-thumbnails-compact-editor-uploader']/span[@class='style-scope ytcp-thumbnails-compact-editor-uploader']")))
        thumnail_input = driver.find_element_by_xpath('//input[@type="file"]')
        thumnail_input.send_keys(thumbnail_path)

    fungsi_tulis(id_datasource,"Setting Menu Anak anak")
    WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.NAME, 'VIDEO_MADE_FOR_KIDS_NOT_MFK'))).click()

    sleep(5)

    fungsi_tulis(id_datasource,"Mengarah Ke Step 3")
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 100).until(EC.element_to_be_clickable((
        By.ID, "step-badge-3"))))

    sleep(10)

    ''' 
    Setting schedule
    1 = Aktif
    2 = Nonaktif / langsung publish
    '''

    upload_time = ""
    if STATUS == 1:
        fungsi_tulis(id_datasource,"Setting Penjadwalan")
        menit = [0,15,30,45]
        today = datetime.now()

        upload_time = datetime(today.year, today.month, today.day, 0, 0)
        upload_time = upload_time+timedelta(days=random.randrange(1,RANDOM_HARI),hours=random.randrange(1,23),minutes=random.choice(menit))

        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.NAME, "SCHEDULE"))).click()
        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#datepicker-trigger > ytcp-dropdown-trigger:nth-child(1)"))).click()
        date_input: WebElement = driver.find_element_by_xpath("//ytcp-date-picker//iron-input/input[@class='style-scope tp-yt-paper-input']")
        sleep(5)
        date_input.clear()

        # Setting tanggal upload
        # Transform date into required format: Mar 19, 2021
        date_input.send_keys(upload_time.strftime("%b %d, %Y"))
        date_input.send_keys(Keys.RETURN)

        sleep(10)

        # Setting jam upload
        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//ytcp-form-input-container[@id='time-of-day-container']//iron-input/input[@class='style-scope tp-yt-paper-input']"))).click()
        time_list = driver.find_elements_by_css_selector("tp-yt-paper-item.tp-yt-paper-item")
        # Transform time into required format: 8:15 PM
        time_str = upload_time.strftime("%I:%M %p").strip("0")
        time = [time for time in time_list[2:] if time.text == time_str][0]
        time.click()

        waktu_str = upload_time.strftime("%d/%m/%Y %H:%M:%S")
        fungsi_tulis(id_datasource,f"Setting Penjadwalan Done | Jadwal {waktu_str}")

        sleep(10)
    else:
        #Untuk publish 
        fungsi_tulis(id_datasource,"Memilih untuk langsung publish")
        WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//tp-yt-paper-radio-button[@class='style-scope ytcp-video-visibility-select'][3]"))).click()
        # untuk menekan tombol save
        sleep(10)


    fungsi_tulis(id_datasource,"Wait Prosess")    
    _wait_for_processing(id_datasource,driver)
    fungsi_tulis(id_datasource,"Proses Berhasil")

    url_video_upload = WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.CLASS_NAME, "video-url-fadeable"))).get_attribute("innerText")

    sleep(10)
    # done
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((
        By.XPATH, "//ytcp-button[@id='done-button']"))))

    # close floating
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((
        By.XPATH, "//div[@class='footer style-scope ytcp-dialog']/ytcp-button[@id='close-button']/div[@class='label style-scope ytcp-button']"))).click()
    
    if upload_time == "":
        upload_time = datetime.now()

    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    upload_time = upload_time.strftime("%d/%m/%Y %H:%M:%S")

    # Kirim laporan ke log
    fungsi_tulis(id_datasource,f"Berhasil upload video {title} dengan URL {url_video_upload}")

    fungsi_tulis(id_datasource,"Jeda {} detik".format(JEDA_UPLOAD))
    sleep(JEDA_UPLOAD)

    # Panggil fungsi update laporan ke database
    updatestatusupload(id_datasource,2,url_video_upload,dt_string,upload_time)
    
    # Kirim laporan ke telegram
    text_tg = f"Sukses Upload Video\n\nWaktu upload : {dt_string}\nSchedule : {upload_time}\nJudul : {title}\nURL : {url_video_upload}"
    text_tg = urllib.parse.quote_plus(text_tg)
    telegram_send(text_tg)

def ytb_upload_from_database():
    """ Setting Web Driver """
    driver = webdriver.Chrome(CHROME)

    """ Setting kolom, untuk mempermudah """
    datasource = json.loads(getlistrunbot())[0]
    id_datasource = datasource["id"]
    judul_video = datasource["judul_video"]
    path_video = datasource["path_video"]
    diskripsi_video = datasource["diskripsi_video"].replace("[enter]","\n")
    setting_thumbnail = datasource["setting_thumbnail"]
    path_thumbnail = datasource["path_thumbnail"]
    path_cookies = datasource["path_cookies"]

    updatestatusds(id_datasource,1,"","")
    fungsi_tulis(id_datasource,"upload video dgn id ==> {} | {}".format(id_datasource, judul_video))
    try:
        login_using_cookie_file(driver,path_cookies,id_datasource)
        fungsi_tulis(id_datasource,"Mencoba login dengan cookies")
        driver.get("https://studio.youtube.com")
        fungsi_tulis(id_datasource,"Done Login Cookies")
        upload_file(
            driver,
            path_video = path_video,
            title = judul_video,
            description = diskripsi_video,
            setting_thumbnail = setting_thumbnail,
            thumbnail_path = path_thumbnail,
            id_datasource = id_datasource
            )
        driver.quit()
        fungsi_tulis(id_datasource,"Upload Selesai video ke --> {} | {}".format(id_datasource, judul_video))
    except Exception as e:
        fungsi_tulis(id_datasource,"Error saat memproses")
        keterangan = str(e).replace("\n","").replace("'","").replace('"','')
        fungsi_tulis(id_datasource,keterangan)
        updatestatusds(id_datasource,3,"",keterangan)
        driver.quit()